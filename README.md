# Exercise by monitoring using Grafana and Prometheus.
Vagrant file creates two VM - with Laravel dockerised application and montoring server. 

On the server installed node_exporter and cAdvisor for monitoring OS and docker containers.  
Monitoring server contains Grafana and Prometheus. After launch, you can go to hostname:3000 to Grafana. Select Promehteus data source and import dashboard from this repository/grafana.

| Port | Resource    |
| ------ |-------------|
| 8081 | application |
| 3306 | DB (Mysql)  |
| 3000 | Grafana     |
| 9090 | Prometheus  |
